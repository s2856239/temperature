package com.example.demo;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


public class HelloServlet extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private FahrenheitProgram fahrenheitProgram;

    public void init() throws ServletException {
        fahrenheitProgram = new FahrenheitProgram();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Book Quote";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius temperature: " +
                request.getParameter("Celsius") + "\n" +
                "  <P>Conversion: " +
                Double.toString(fahrenheitProgram.convert(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
    }


}
